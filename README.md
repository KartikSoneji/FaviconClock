# Favicon Clock
A fully functional analog clock using SVG, in the favicon!  
Try it out: http://kartiksoneji.gitlab.io/FaviconClock

## How it works
This page renders the clock using `<svg>` elements, then converts the tag to a data URI and sets it as the favicon href.  
For more details, please refer to [`script.js`](script.js).

## Screenshot
![screenshot](screenshot.png)