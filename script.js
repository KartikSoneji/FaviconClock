let
	favicon,
	
	clock,
	hourHand,
	minuteHand,
	secondHand,
	
	continuousSecondHand = true;

window.addEventListener("load", e => {
	favicon = document.querySelector("[rel = icon]");
	
	clock = document.querySelector("#clock");
	
	hourHand = document.querySelector("#hourHand")
	minuteHand = document.querySelector("#minuteHand")
	secondHand = document.querySelector("#secondHand")
	
	tick(true);
	setInterval(tick, 10);
});

function tick(firstRun){
	let now = new Date();
	
	setTime(now);
	if(now.getSeconds() < 2 || firstRun)
		favicon.href = svgToDataUri(clock);
}

function setTime(time = new Date()){
	let
		h = time.getHours() % 12,
		m = time.getMinutes(),
		s = time.getSeconds(),
		ms = time.getMilliseconds();
	
	if(continuousSecondHand)
		s += ms/1000;
	
	setRotation(hourHand, (h + m/60) * 30);
	setRotation(minuteHand, (m + s/60) * 6);
	setRotation(secondHand, s * 6 - 90);
}

function setRotation(e, r){
	e.setAttribute("transform", "rotate(" + r + ")");
}

function svgToDataUri(svg){
	return "data:image/svg+xml," + encodeURIComponent(svg.outerHTML);
}
